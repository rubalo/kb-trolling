# KB-Trolling

Keep inviting a user on a slack channel
- You must be on that channel

## Get Slack token
https://api.slack.com/custom-integrations/legacy-tokens

## Create configuration
```
cat > ~/.cronfile << EOF
export SLACK_TOKEN=<You_slack_token>
export USER_ID=<Slack_user_id>
export CHANNEL_ID=<Slack_channel_id>
EOF
```

## Update configuration

Edit the ~/.cronfile to enter the right values

## Test the program
```
. ~/.cronfile; ./invite_kb.sh
```

## Add it to your crontab
```
crontab -l | { cat; echo "* * * * * . ~/.cronfile; $(pwd)/invite_kb.sh"; } | crontab -
```
