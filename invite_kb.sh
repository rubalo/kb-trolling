#!/bin/bash

sleep $[ ( $RANDOM % 10 )  + 1 ]s
curl -X POST -H "Authorization: Bearer $SLACK_TOKEN" \
-H 'Content-type: application/json' \
--data "{'channel':'$CHANNEL_ID','user':'$USER_ID'}" \
https://slack.com/api/channels.invite
